import neat
import game

ITER = -1

def eval_genomes(genomes, config):
    global ITER
    ITER += 1
    g = game.Game(100)
    for genome_id, genome in genomes:
        pos = g.random_empty()
        net = neat.nn.FeedForwardNetwork.create(genome, config)
        creature = game.Creature(pos, genome, net)
        g.add_creature(creature)
    if not ITER % 1:
        v = game.Visualizer(g)
        v.show()
    else:
        g.run()


# Load configuration.
config = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                     neat.DefaultSpeciesSet, neat.DefaultStagnation,
                     'config-feedforward')

# Create the population, which is the top-level object for a NEAT run.
p = neat.Population(config)

# Add a stdout reporter to show progress in the terminal.
p.add_reporter(neat.StdOutReporter(False))

# Run until a solution is found.
winner = p.run(eval_genomes)

# Display the winning genome.

# Show output of the most fit genome against training data.
