import numpy as np
from matplotlib import pyplot as plt
import pygame
import random 




class Thing:
    empty = 0
    spike = 1
    food = 2
    creature = 3
    count = 4

    colors = np.array([
        (255,255,255),
        (255,0,0),
        (0,255,0),
        (0,0,255),
        ])

    ID = 0
    id_count = 0
    def __init__(self):
        Thing.id_count += 1
        self.ID = Thing.id_count

class Action:
    none = -1
    move = 0

    count = 1

    right = 0
    left = 1
    up = 2
    down = 3

    cross = [(1,0),(-1,0),(0,-1),(0,1)]

class Creature(Thing):
    def __init__(self, pos, genome, net):
        super().__init__()
        self.pos = pos
        self.health = 10
        self.genome = genome
        self.net = net

    def get_action(self, view):
        self.health -= 1
        data = list(view) + [self.health,random.random()%2 - 1]
        output = self.net.activate(data)
        action = np.argmax(output[:Action.count])
        dirr = Action.cross[np.argmax(output[Action.count:])]
        return (action, dirr)

    def die(self, step):
        self.health = 0
        self.genome.fitness = step
        pass

class Game:
    def __init__(self, size):
        self.vision = 2
        self.size = size
        self.full_size = size + self.vision * 2
        self.grid = np.zeros([self.full_size, self.full_size, Thing.count]).astype(int)
        self.grid[:,:,Thing.spike] = 1
        self.grid[self.vision:self.full_size - self.vision,self.vision:self.full_size - self.vision,Thing.spike] = 0
        self.grid[self.vision:self.full_size - self.vision,self.vision:self.full_size - self.vision,Thing.empty] = 1
        self.thing_grid = [[None] * size for i in range(size)]
        self.things = {}
        self.dead = []
        self.step = 0

    def random_empty(self):
        pos = np.random.randint(self.size,size=2)
        while self.geti(pos) != Thing.empty:
            pos = np.random.randint(self.size,size=2)
        return pos

    def add_creature(self, creature):
        pos = creature.pos
        self.seti(creature.pos,Thing.creature)
        self.thing_grid[pos[0]][pos[1]] = creature
        self.things[creature.ID] = creature
        for i in range(10):
            self.drop_food()

    def geti(self, pos):
        if not (0 <= min(pos) and max(pos) < self.size): return Thing.spike
        return np.argmax(self.grid[self.vision+pos[0],self.vision+pos[1],:])

    def seti(self, pos, thing):
        if not (0 <= min(pos) and max(pos) < self.size): return
        self.grid[self.vision+pos[0],self.vision+pos[1],:] = np.eye(Thing.count)[thing]

    def kill(self, creature):
        self.thing_grid[creature.pos[0]][creature.pos[1]] = None
        self.seti(creature.pos, Thing.food)
        creature.die(self.step)

    def get_view(self, pos):
        return self.grid[
                pos[0]:pos[0]+2*self.vision+1,
                pos[1]:pos[1]+2*self.vision+1,
                :].flatten()

    def drop_food(self):
        pos = self.random_empty()
        self.seti(pos,Thing.food)

    def move(self):
        self.step += 1
        for i in range(int(len(self.things) * 0.09)):
            self.drop_food()
        for ID,thing in self.things.items(): 
            if thing.health <= 0: self.kill(thing)
        self.things = dict(filter(lambda x : x[1].health>0, self.things.items()))
        for ID,thing in self.things.items():
            action,dirr = thing.get_action(self.get_view(thing.pos))
            new_pos = thing.pos + np.array(dirr)
            new_pos_thing = self.geti(new_pos)
            if action == Action.move: 
                if new_pos_thing == Thing.empty:
                    self.seti(thing.pos,Thing.empty)
                    thing.pos = new_pos
                    self.seti(thing.pos,Thing.creature)
                elif new_pos_thing == Thing.spike:
                    thing.health = 0
                elif new_pos_thing == Thing.food:
                    self.seti(thing.pos,Thing.empty)
                    thing.pos = new_pos
                    self.seti(thing.pos,Thing.creature)
                    thing.health += 10
            elif action == Action.spike:
                if new_pos_thing == Thing.empty:
                    self.seti(new_pos,Thing.spike)
            elif action == Action.eat:
                if new_pos_thing == Thing.food:
                    self.seti(new_pos, Thing.empty)
                    thing.health = min(thing.health + 10, 100)
            elif action == Action.dig:
                if new_pos_thing == Thing.spike:
                    self.seti(new_pos, Thing.empty)
            elif action == Action.push:
                pass

    def run(self):
        while len(self.things):
            self.move()
                    

class Visualizer:
    def __init__(self, game):
        self.game = game
        pygame.init()
        self.scale = int(600/game.size)
        self.size = np.ones(2) * self.scale * game.full_size

        self.screen = pygame.display.set_mode(self.size)
        pygame.display.set_caption('Game')
        self.clock = pygame.time.Clock()

    def show(self):
        while len(self.game.things):
            for event in pygame.event.get():
                if event.type == pygame.QUIT: 
                    return

            self.game.move()
            x = np.argmax(self.game.grid,2)
            surface = pygame.surfarray.make_surface(Thing.colors[x])
            surface = pygame.transform.scale(surface, self.size)
            self.screen.blit(surface, (0, 0))
            pygame.display.update()
            #self.clock.tick(60)

if __name__ == "__main__":
    g = Game()
    for i in range(g.size):
        g.add_creature((i,i))
    v = Visualizer(g)
    v.show()



